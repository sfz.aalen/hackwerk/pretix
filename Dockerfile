FROM docker.io/pretix/standalone:2025.2
USER root
RUN pip3 install pretix-dbvat
RUN pip3 install pretix-zugferd
USER pretixuser
RUN cd /pretix/src && make production